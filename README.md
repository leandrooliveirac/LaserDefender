# LaserDefender
A Top-Down Space Shooter developed with C# and Unity for learning purposes:

* Game Design
* Coroutines
* GameObject Shredder
* Scriptable Object
* Layer Collision Matrix
* Scrolling Background


## Game features

* Scrolling background
* Enemy waves
* Health and score
* Basic SFX and VFX


## How To Build / Compile
This is a Unity project, therefore it requires Unity3D to build. Clone or download this repo, and navigate to `Assets > Scenes` then open any `.unity` file.
